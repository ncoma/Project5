﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary3.pojos
{
    class Rectangle
    {
        public int width { get; set; }
        public int height { get; set; }

        public Rectangle(int width, int height)
        {
            this.width = width;
            this.height = height;
        }


    }
}
